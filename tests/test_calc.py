import os
import time
import notes
import unittest
from calc import Calculator

class TestCalculator(unittest.TestCase):

    def setUp(self):
        self.calc = Calculator()

    def test_add(self):
        self.calc.add(2)
        self.assertEqual(self.calc.value, 2)
        
    def test_subtract(self):
        self.calc.subtract(5)
        self.assertEqual(self.calc.value, -5)
        
    def test_multiply(self):
        self.calc.multiply(3)
        self.assertEqual(self.calc.value, 0)
        
    def test_divide(self):
        with self.assertRaises(ZeroDivisionError):
            self.calc.divide(0)
            
if __name__ == '__main__':
    unittest.main()